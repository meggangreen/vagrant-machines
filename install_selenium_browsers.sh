##############
# SELENIUM + BROWSERS + DRIVERS   # Python3 is required!
##############
# Using set -e will make the script exit if any line gives as non-zero return
set -e

# exit without error if Py3 not installed -- https://stackoverflow.com/a/38485534
if [[ ! command -v python3 &>/dev/null ]]; then
    echo -e "\n\n----- Will not install Selenium etc: Python3 not installed -----"
    echo -e "add Py3 installer script to Vagrantfile provisioners; use 'vagrant"
    echo -e "provision' to rerun installer scripts without rebuilding box\n\n"
    exit 0
fi

echo -e "\n\n----- Installing Selenium, Firefox, and GeckoDriver -----"
sudo apt-get update

# Selenium
sudo -H pip3 install selenium

################
# webdrivers: firefox
################
# install regular browser -- want to find a mini version
sudo apt-get install -y firefox

# download webdriver -- https://github.com/mozilla/geckodriver/releases
wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
# response: [timestamp]  ‘geckodriver-v0.24.0-linux64.tar.gz’ saved [2896664/2896664]

# extract
tar xvzf geckodriver-v0.24.0-linux64.tar.gz
# response: geckodriver

# clean up
rm geckodriver-v0.24.0-linux64.tar.gz

# make executable -- optional
# chmod +x geckodriver
# response: none

# move to /usr/local/bin
sudo mv geckodriver /usr/local/bin/

# save in PATH var -- optional
# export PATH=$PATH:/usr/local/bin/geckodriver

# in python -- https://realpython.com/modern-web-automation-with-python-and-selenium/
# from selenium.webdriver import Firefox
# from selenium.webdriver.firefox.options import Options
# options = Options()
# options.headless = True
# ff = Firefox(options=options)
# # do stuff with browser 'ff'
# ff.get("http://meggangreen.com")
# ff.title  # response: ':: meggangreen ::'
# ff.close()


################
# webdrivers: chrome -- untested
################
# add the google-chrome repository
# wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

# # get most updated package
# sudo apt update
# sudo apt install google-chrome-stable
# # beta, stable, or release by substitution

# # download webdriver -- https://sites.google.com/a/chromium.org/chromedriver/downloads
# wget https://chromedriver.storage.googleapis.com/2.46/chromedriver_linux64.zip

# # extract
# unzip chromedriver_linux64.zip

# # clean up
# rm chromedriver_linux64.zip

# # move
# sudo mv chromedriver /usr/local/bin/
