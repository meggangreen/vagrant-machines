#############################
# LINUX SETUP SCRIPT
#############################
#
# Meggan M Green <email@meggan.green>
#
# Based on 2015-2017 Hackbright (HB) script written by
# Joel Burton <joel@hackbrightacademy.com> and
# Katie Byers <katiebyers@hackbrightacademy.com>
#
# This script is to be run by Vagrant as the first step of provisioning when
# first running 'vagrant up'. Only things specific for HB Linux installs should
# be included.
#

# All Meggan's boxes are Python based with PostgreSQL. Additional optional
# are commented out or removed from the individual set up scripts.

# Using set -e will make the script exit if any line gives as non-zero return
set -e

##############
# LINUX GENERAL
##############
# ensure that environment and Postgres default to UTF-8
echo "LANG=en_US.UTF-8" > /etc/default/locale
echo "LANGUAGE=en_US.UTF-8:" >> /etc/default/locale

# update package listings
echo -e "----- Updating package listings -----"
sudo apt-get update

# install useful Linux packages
echo -e "----- Installing Linux packages, incl Git and SQLite3 -----"
sudo apt-get install -y git sqlite3 libxml2-dev libxslt1-dev libffi-dev libssl-dev

##############
# PYTHON   # Python 3.6 is default on Ubuntu 17.10
##############
# install necessary Python packages
echo -e "----- Installing Python packages, pip, virtualenv, ipython -----"
sudo apt-get install -y python3-pip python3-dev python3-virtualenv
sudo pip3 install upgrade pip

# upgrade to most recent pip
# sudo pip3 install -U pip  # ?? this changed my pip name?
sudo pip3 install ipython
# install additional Python packages
sudo pip3 install numpy

# drop excluded Python packages here:
# sudo apt-get install -y
# sudo pip3 install python-twitter notebook

##############
# POSTGRESQL
##############
# install PostgreSQL packages
# echo -e "----- Installing PostgreSQL packages -----"
# sudo apt-get install -y postgresql-client postgresql postgresql-contrib postgresql-plpython postgresql-server-dev-9.6
# sudo pip3 install psycopg2-binary

##############
# NODEJS & NPM
##############
# get nodejs - nodejs site recomend using curl over 'add-apt-repository'
# see https://nodejs.org/en/download/package-manager/
# running curl in 's' silent and 'L' location - if 3## in response resource
# has been moved and curl will look at the redirect location.
# echo -e "----- Getting and installing Nodejs and NPM packages -----"
# curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
# sudo apt-get install -y nodejs npm

# display success message
echo
echo "*************************************"
echo "Linux setup complete. No errors encountered."
echo "*************************************"
echo
