#############################
# JAVA SETUP SCRIPT
#############################
#
# Meggan M Green <email@meggan.green>
#
# Based on the CLI instructions from the OpenJDK website
#

# Using set -e will make the script exit if any line gives as non-zero return
set -e

# Install OpenJava 8
echo -e "----- Installing Java 8 ------"
sudo add-apt-repository ppa:openjdk-r/ppa -y
sudo apt-get update
sudo apt-get -y install openjdk-8-jdk
sudo update-alternatives --config java

# print success message
echo
echo "*************************************"
echo "Java setup complete. No errors encountered"
echo "*************************************"
echo
