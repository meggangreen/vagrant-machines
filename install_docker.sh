##############
# DOCKER -- CE using a repository -- Vagrant has a Docker provisioner, but docs are sparse
##############
# Using set -e will make the script exit if any line gives as non-zero return
# set -e

echo -e "\n\n----- Installing Docker and its dependencies -----"
sudo apt-get update

# supporting packages, which are probably already installed
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# docker's gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# set up repo
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# install CE
sudo apt-get update
sudo apt-get install -y docker-ce

# make docker group (should exist) and add vagrant to it which lets 'vagrant' run docker commands; otherwise require 'sudo'
# this may be the command which gives non-zero exit and causes the script to abort when 'set -e' commanded
sudo groupadd docker
sudo usermod -aG docker vagrant

# run 'sudo reboot' to restart
# confirm with '$ docker run --rm hello-world'
