#############################
# LINUX SETUP SCRIPT
#############################
#
# Meggan M Green <email@meggan.green>
#
# Based on 2015-2017 Hackbright (HB) script written by
# Joel Burton <joel@hackbrightacademy.com> and
# Katie Byers <katiebyers@hackbrightacademy.com>
#
# This script is to be run by Vagrant as the first step of provisioning when
# first running 'vagrant up'.
#

# Using set -e will make the script exit if any line gives as non-zero return
set -e

##############
# LINUX GENERAL
##############
# ensure that environment and Postgres default to UTF-8
echo -e "LANG=en_US.UTF-8" > /etc/default/locale
echo -e "LANGUAGE=en_US.UTF-8:" >> /etc/default/locale

# update package listings
echo -e "----- Updating package listings -----"
sudo apt-get update

# install useful Linux packages
echo -e "\n\n----- Installing Linux packages, incl Git and SQLite3 -----"
sudo apt-get install -y git sqlite3 libxml2-dev libxslt1-dev libffi-dev libssl-dev

##############
# PYTHON   # Python 3.6 is default on Ubuntu 17.10
##############
echo -e "\n\n----- Installing Python packages, pip, virtualenv, ipython -----"
sudo apt-get install -y python3-pip python3-dev python3-virtualenv

# upgrade to most recent pip
sudo -H pip3 install -U pip  # '-H' makes it not break pip/pip3 http://manpages.ubuntu.com/manpages/bionic/en/man8/sudo.8.html
# install additional Python packages
sudo pip3 install ipython numpy requests

# drop excluded Python packages here:
# sudo apt-get install -y
# sudo pip3 install python-twitter notebook pandas


##############
# UPGRADE PACKAGES
##############
echo -e "\n\n----- Running Upgrades -----"
sudo apt-get upgrade -y

# display success message
echo -e "\n\n*************************************"
echo -e "Linux setup complete. No errors encountered."
echo -e "    -- Remember to run 'vagrant reload'"
echo -e "*************************************\n"
