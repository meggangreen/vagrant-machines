##############
# PYTHON   # Python 3.6 is default on Ubuntu 17.10+
##############
# Using set -e will make the script exit if any line gives as non-zero return
# set -e

# Upgrade to Python 3.10 per https://cloudbytes.dev/snippets/upgrade-python-to-latest-version-on-ubuntu-linux
pyv=$(python3 --version)
if [ ! "$pyv" == "Python 3.10.1" ]; then
    echo -e "\n\n----- Replacing Python version 3.6 with 3.10 -----"
    # install 3.10
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt-get update
    sudo apt-get install -y python3.10

    # update default lib
    sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
    sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 2
    # sudo update-alternatives --config python3  # verify that 3.10 is python3 in auto mode

    # remove 3.6
    sudo apt-get remove -y python3.6
    sudo apt-get autoremove -y

    # fix pip and venv
    sudo apt-get install -y python3.10-distutils
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python3.10 get-pip.py
    sudo apt-get install -y python3.10-venv

    # remove python-apt (if there)
    sudo apt-get remove -y --purge python3-apt
    sudo apt-get autoremove -y
fi

# install additional Python packages
echo -e "\n\n----- Installing iPython and requests -----"
pip install ipython requests

# to create virtual env with venv:
# python3 -m venv env
# source env/bin/activate
# deactivate

# drop excluded Python packages here:
# pip install python-twitter notebook pandas numpy
