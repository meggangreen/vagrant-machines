##############
# LINUX GENERAL
##############

# Using set -e will make the script exit if any line gives as non-zero return
# set -e

echo -e "\n\n----- Importing Bashrc -----"
sudo cp vagrant/bash.bashrc /etc/bash.bashrc
sudo printf "\n\nsource /etc/profile" >> .bashrc
source /etc/profile

echo -e "\n\n----- Installing Linux packages, incl Git and SQLite3 -----"

# update package listings
sudo apt-get update && apt-get upgrade

# install useful Linux packages
# remove -y flag because of a service restart confirmation for libssl1.1:amd64 (1.1.1-1ubuntu2.1~18.04.14)
# untested in auto provisioning script
sudo apt-get install git htop sqlite3 unzip libxml2-dev libxslt1-dev libffi-dev libssl-dev
