##############
# POSTGRESQL
##############
# Using set -e will make the script exit if any line gives as non-zero return
set -e

echo -e "\n\n----- Installing PostgreSQL packages -----"
# install PostgreSQL packages
sudo apt-get install -y postgresql-client postgresql postgresql-contrib postgresql-plpython-10 postgresql-server-dev-10
sudo -H pip install psycopg2-binary
