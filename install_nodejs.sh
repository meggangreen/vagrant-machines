##############
# NODEJS & NPM
##############
# Using set -e will make the script exit if any line gives as non-zero return
set -e

echo -e "\n\n----- Installing Nodejs via NVM -----"
# NVM is a version manager; see notes about updating bash profile and `use`
#   the script does not install node
#   https://github.com/nvm-sh/nvm#install--update-script
#   check for latest or otherwise correct version
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
# install node + npm https://github.com/nvm-sh/nvm#usage
# close and reopen terminal or enter
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install node  # installs latest version, which becomes default version


